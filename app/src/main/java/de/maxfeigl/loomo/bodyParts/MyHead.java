package de.maxfeigl.loomo.bodyParts;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import java.util.Timer;
import java.util.TimerTask;

import de.maxfeigl.emotions.bodyparts.LoomoHead;
import de.maxfeigl.emotions.helpers.AppConstants;
import de.maxfeigl.emotions.helpers.JSONConverter;

/**
 * Created by Max on 17.01.2018.
 */

public class MyHead extends LoomoHead {
    private Boolean move;
    private boolean even = true;
    private int shakeHeadCount;

    public MyHead(Context context, JSONConverter jsonConverter) {
        super(context, jsonConverter);
    }

    /**
     * @param velocity velocity with which to look ranging from -Math.PI to Math.PI
     * @param duration duration of motion in milliseconds
     */
    @Override
    protected void lookUp(float velocity, int duration) {
        if (getMovementSpeed() == AppConstants.FAST_MOVEMENT_SPEED)
            duration = duration / 4;
        else
            duration = duration * 2;

        move = true;
        while (move) {
            getmHead().setPitchAngularVelocity(velocity);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    move = false;
                }
            }, duration);
        }
        getmHead().setPitchAngularVelocity(0);
    }

    /**
     * @param velocity velocity with which to look ranging from -Math.PI to Math.PI
     * @param duration duration of motion in milliseconds
     */
    @Override
    protected void lookDown(float velocity, int duration) {
        if (getMovementSpeed() == AppConstants.FAST_MOVEMENT_SPEED)
            duration = duration / 4;
        else
            duration = duration * 2;
        move = true;
        while (move) {
            getmHead().setPitchAngularVelocity(-velocity);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    move = false;
                }
            }, duration);
        }
        getmHead().setPitchAngularVelocity(0);
    }

    /**
     * @param velocity velocity with which to look ranging from -Math.PI to Math.PI
     * @param duration duration of motion in milliseconds
     */
    @Override
    protected void lookLeft(float velocity, int duration) {
        if (velocity > Math.PI) velocity = (float) Math.PI;
        move = true;
        while (move) {
            getmHead().setYawAngularVelocity(velocity);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    move = false;
                }
            }, duration);
        }
        getmHead().setYawAngularVelocity(0);
    }

    /**
     * @param velocity velocity with which to look ranging from -Math.PI to Math.PI
     * @param duration duration of motion in milliseconds
     */
    @Override
    protected void lookRight(float velocity, int duration) {
        if (velocity > Math.PI) velocity = (float) Math.PI;
        move = true;
        while (move) {
            getmHead().setYawAngularVelocity(-velocity);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    move = false;
                }
            }, duration);
        }
        getmHead().setYawAngularVelocity(0);
    }

    @Override
    protected void shakeHead() {
        shakeHeadCount = 0;
        final Handler handler = new Handler(Looper.getMainLooper());
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (even) {
                    lookLeft(getVelocity() * 2, getDuration() / 4);
                } else {
                    lookRight(getVelocity() * 2, getDuration() / 4);
                }
                even = !even;
                shakeHeadCount++;
                if (shakeHeadCount < 4)
                    handler.postDelayed(this, getDuration() / 4);
            }
        };

        handler.postDelayed(runnable, 0);
    }
}
