package de.maxfeigl.emotions.bodyparts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.AppConstants;


/**
 * Created by Max on 03.01.2018.
 */

public class Movement extends BodyPart {
    private JSONArray movementObject;
    private int movementSpeed = AppConstants.SLOW_MOVEMENT_SPEED;
    private int delayBetweenMovements = AppConstants.FEW_MOVEMENTS;
    private ArrayList<String> fallbackMovements = new ArrayList<>();


    public void setMovement(JSONArray object) {
        movementObject = object;
        try {
            setMovementSpeed();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setMovementSpeed() throws JSONException {
        for (int i = 0; i < movementObject.length(); i++) {
            switch (movementObject.get(i).toString()) {
                case "schnelle Bewegungen":
                    movementSpeed = AppConstants.FAST_MOVEMENT_SPEED;
                    break;
                case "langsame Bewegungen":
                    movementSpeed = AppConstants.SLOW_MOVEMENT_SPEED;
                    break;
                case "wenig Bewegungen":
                    delayBetweenMovements = AppConstants.FEW_MOVEMENTS;
                    break;
                case "viele Bewegungen":
                    delayBetweenMovements = AppConstants.MANY_MOVEMENTS;
            }
        }
    }

    @Override
    protected void executeFunctionForName(String name) {

    }

    @Override
    protected void executeReverseFunctionForName(String name) {

    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {

    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        return true;
    }

    @Override
    public int getMovementSpeed() {
        return movementSpeed;
    }

    @Override
    public void setMovementSpeed(int movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    @Override
    public int getDelayBetweenMovements() {
        return delayBetweenMovements;
    }

    @Override
    public void setDelayBetweenMovements(int delayBetweenMovements) {
        this.delayBetweenMovements = delayBetweenMovements;
    }
}
