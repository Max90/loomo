package de.maxfeigl.emotions.bodyparts;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.segway.robot.sdk.base.bind.ServiceBinder;
import com.segway.robot.sdk.locomotion.head.Head;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.AppConstants;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 13.12.2017.
 */

public abstract class LoomoHead extends BodyPart {
    private float velocity;
    private int duration;
    private Context context;
    private Head mHead;
    private boolean bindSuccess = false;
    private boolean even = true;

    public LoomoHead(Context context, JSONConverter jsonConverter) {
        super();
        this.context = context;
        reinitializeHead(jsonConverter, null);
    }

    public void setSpeeds() {
        setMovementSpeed();
        setMovementDuration();
    }

    private void setMovementDuration() {
        if (duration == AppConstants.MANY_MOVEMENTS)
            duration = AppConstants.MANY_MOVEMENTS / 3;
        else
            duration = AppConstants.FEW_MOVEMENTS / 3;
    }

    private void setMovementSpeed() {
        if (movementSpeed == AppConstants.FAST_MOVEMENT_SPEED)
            velocity = 3;
        else
            velocity = 0.25f;
    }

    /**
     * resets all arrayLists back to empty lists
     * needs to be different than other body parts because bindService may fail otherwise
     *
     * @param jsonConverter
     */
    public void reinitializeHead(JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
        this.mandatoryCheck = mandatoryCheck;
    }

    public void bindHeadService() {
        mHead = Head.getInstance();
        bindService(mHead);
    }

    private void bindService(final Head mHead) {
        mHead.bindService(context, new ServiceBinder.BindStateListener() {
            @Override
            public void onBind() {
                Log.d("Loomo", "head onBind() called");
                mHead.setMode(Head.MODE_ORIENTATION_LOCK);
                bindSuccess = true;
            }

            @Override
            public void onUnbind(String reason) {
                Log.d("Loomo", "onUnbind() called with: reason = [" + reason + "]");
            }
        });
    }

    protected abstract void lookUp(float velocity, int duration);

    protected abstract void lookDown(float velocity, int duration);

    protected abstract void lookLeft(float velocity, int duration);

    protected abstract void lookRight(float velocity, int duration);

    protected abstract void shakeHead();

    @Override
    protected void executeFunctionForName(String name) {
        Log.i("movements", name);
        switch (name) {
            case "Kopf nach links geneigt":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        lookLeft(velocity, duration);
                    }
                });
                break;
            case "Kopf nach rechts geneigt":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        lookRight(velocity, duration);
                    }
                });
                break;
            case "Kopf nach oben geneigt":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        lookUp(velocity, duration);
                    }
                });
                break;
            case "Kopf nach unten geneigt":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        lookDown(velocity, duration);
                    }
                });
                break;
            case "Kopfschütteln":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        shakeHead();
                    }
                });
                break;
        }
        super.addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(name);
    }

    @Override
    protected void executeReverseFunctionForName(String name) {
        Log.i("movements", "reverse " + name);
        switch (name) {
            case "Kopf nach links geneigt":
                lookRight(velocity, duration);
                break;
            case "Kopf nach rechts geneigt":
                lookLeft(velocity, duration);
                break;
            case "Kopf nach oben geneigt":
                lookDown(velocity, duration);
                break;
            case "Kopf nach unten geneigt":
                lookUp(velocity, duration);
                break;
            case "Kopfschütteln":
                //have to do nothing since head is in starting position
                break;
        }
    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {
        super.setBodyPart(object);
    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        switch (name) {
            case "Kopf nach links geneigt":
                break;
            case "Kopf nach rechts geneigt":
                break;
            case "Kopf nach oben geneigt":
                break;
            case "Kopf nach unten geneigt":
                break;
            case "Kopfschütteln":
                shakeHead();
                break;
            default:
                if (checkForFallback)
                    super.checkIfFallbackPossible(name);
                else
                    return false;
                break;
        }
        return true;
    }

    public Head getmHead() {
        return mHead;
    }

    public float getVelocity() {
        return velocity;
    }

    public int getDuration() {
        return duration;
    }

    public Context getContext() {
        return context;
    }

    public boolean isBindSuccess() {
        return bindSuccess;
    }

    public boolean isEven() {
        return even;
    }
}
