package de.maxfeigl.emotions.bodyparts;

import android.content.Context;
import android.util.Log;
import android.view.View;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 13.12.2017.
 */

public abstract class Eyebrow extends BodyPart {
    private static final int EYEBROW_LOWERING_IN_PX = 20;
    private static final int EYEBROW_CONTRACTION_IN_PX = 35;
    private View leftEyebrow;
    private View rightEyebrow;

    public Eyebrow(View leftEyebrow, View rightEyebrow, Context context, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super();
        this.leftEyebrow = leftEyebrow;
        this.rightEyebrow = rightEyebrow;
        this.mandatoryCheck = mandatoryCheck;
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
    }

    public static int getEyebrowLoweringInPx() {
        return EYEBROW_LOWERING_IN_PX;
    }

    public static int getEyebrowContractionInPx() {
        return EYEBROW_CONTRACTION_IN_PX;
    }

    protected abstract void lowerEyebrow(View leftEyebrow, View rightEyebrow);

    protected abstract void reverseLowerEyebrow(View leftEyebrow, View rightEyebrow);

    protected abstract void raiseEyebrow(View leftEyebrow, View rightEyebrow);

    protected abstract void reverseRaiseEyebrow(View leftEyebrow, View rightEyebrow);

    protected abstract void contractEyebrow(View leftEyebrow, View rightEyebrow);

    protected abstract void reverseContractEyebrow(View leftEyebrow, View rightEyebrow);

    protected abstract void raiseInnerEyebrow(View leftEyebrow, View rightEyebrow);

    protected abstract void reverseRaiseInnerEyebrow(View leftEyebrow, View rightEyebrow);

    protected abstract void lowerInnerEyebrow(View leftEyebrow, View rightEyebrow);

    @Override
    protected void executeFunctionForName(String name) {
        Log.i("movements", name);
        switch (name) {
            case "Augenbrauen zusammengezogen":
                contractEyebrow(leftEyebrow, rightEyebrow);
                break;
            case "Augenbrauen gesenkt":
                lowerEyebrow(leftEyebrow, rightEyebrow);
                break;
            case "Augenbrauen erhöht":
                raiseEyebrow(leftEyebrow, rightEyebrow);
                break;
            case "Augenbrauen innere Ecken nach oben gezogen":
                raiseInnerEyebrow(leftEyebrow, rightEyebrow);
                break;
            case "Augenbrauen innere Ecken nach unten gezogen":
                lowerInnerEyebrow(leftEyebrow, rightEyebrow);
                break;
        }
        super.addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(name);
    }

    @Override
    protected void executeReverseFunctionForName(String name) {
        Log.i("movements", "reverse " + name);
        switch (name) {
            case "Augenbrauen zusammengezogen":
                reverseContractEyebrow(leftEyebrow, rightEyebrow);
                break;
            case "Augenbrauen gesenkt":
                reverseLowerEyebrow(leftEyebrow, rightEyebrow);
                break;
            case "Augenbrauen erhöht":
                reverseRaiseEyebrow(leftEyebrow, rightEyebrow);
                break;
            case "Augenbrauen innere Ecken nach oben gezogen":
                reverseRaiseInnerEyebrow(leftEyebrow, rightEyebrow);
                break;
            case "Augenbrauen innere Ecken nach unten gezogen":
                reverseRaiseInnerEyebrow(leftEyebrow, rightEyebrow);
                break;
        }
    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {
        super.setBodyPart(object);
    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        switch (name) {
            case "Augenbrauen zusammengezogen":
                break;
            case "Augenbrauen gesenkt":
                break;
            case "Augenbrauen erhöht":
                break;
            case "Augenbrauen innere Ecken nach oben gezogen":
                break;
            case "Augenbrauen innere Ecken nach unten gezogen":
                break;
            default:
                if (checkForFallback)
                    super.checkIfFallbackPossible(name);
                else
                    return false;
                break;
        }
        return true;
    }
}
