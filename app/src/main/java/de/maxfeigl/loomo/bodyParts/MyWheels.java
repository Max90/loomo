package de.maxfeigl.loomo.bodyParts;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.segway.robot.algo.Pose2D;

import java.util.Timer;
import java.util.TimerTask;

import de.maxfeigl.emotions.bodyparts.Wheels;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.Util;

/**
 * Created by Max on 17.01.2018.
 */

public class MyWheels extends Wheels {
    private boolean drive;
    private int shiverCount;
    private boolean even;
    private boolean move;

    public MyWheels(Context context, JSONConverter jsonConverter) {
        super(context, jsonConverter);
    }

    /**
     * @param velocity velocity ranging from 0 to 3
     * @param duration duration in milliseconds
     */
    @Override
    protected void moveForward(float velocity, int duration) {
        drive = true;
        while (drive) {
            getmBase().setLinearVelocity(velocity);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    drive = false;
                }
            }, duration);
        }
        getmBase().setLinearVelocity(0);
    }

    /**
     * @param velocity velocity ranging from 0 to 3
     * @param duration duration in milliseconds
     */
    @Override
    protected void moveBackwards(float velocity, int duration) {
        drive = true;
        while (drive) {
            getmBase().setLinearVelocity(-velocity);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    drive = false;
                }
            }, duration);
        }
        getmBase().setLinearVelocity(0);
    }

    /**
     * turns Loomo left for about 90°
     *
     * @param velocity velocity with which loomo turns value from 0 to 3
     */
    @Override
    protected void turnLeft(float velocity) {
        Pose2D pose2D = getmBase().getOdometryPose(-1);
        getmBase().setOriginalPoint(pose2D);
        float currentTheta = pose2D.getTheta();
        float targetTheta = (float) (currentTheta + Math.PI / 4);

        // if target > Math.PI the angle restarts at -Math.PI
        if (targetTheta > Math.PI) {
            targetTheta = (float) (-Math.PI + (targetTheta - Math.PI));
        }

        float diff = Util.differenceBetweenAngles(currentTheta, targetTheta);

        while ((targetTheta > currentTheta || !Util.sameSign(currentTheta, targetTheta)) &&
                (diff <= Math.PI)) {
            getmBase().setAngularVelocity(velocity);
            currentTheta = getmBase().getOdometryPose(-1).getTheta();
            diff = Util.differenceBetweenAngles(currentTheta, targetTheta);
        }
        getmBase().setAngularVelocity(0);
    }

    /**
     * turns Loomo right for about 90°
     *
     * @param velocity velocity with which loomo turns value from 0 to 3
     */
    @Override
    protected void turnRight(float velocity) {
        Pose2D pose2D = getmBase().getOdometryPose(-1);
        getmBase().setOriginalPoint(pose2D);
        float currentTheta = pose2D.getTheta();
        float targetTheta = (float) (currentTheta - Math.PI / 4);


        if (targetTheta < -Math.PI) {
            targetTheta = (float) (Math.PI + (targetTheta + Math.PI));
        }

        float diff = Util.differenceBetweenAngles(currentTheta, targetTheta);

        while ((targetTheta < currentTheta || !Util.sameSign(currentTheta, targetTheta)) &&
                (diff <= Math.PI)) {
            getmBase().setAngularVelocity(-velocity);
            currentTheta = getmBase().getOdometryPose(-1).getTheta();
            diff = Util.differenceBetweenAngles(currentTheta, targetTheta);
        }
        getmBase().setAngularVelocity(0);
    }

    @Override
    protected void inchRight(float velocity, int duration) {
        move = true;
        while (move) {
            getmBase().setAngularVelocity(velocity);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    move = false;
                }
            }, duration);
        }
        getmBase().setAngularVelocity(0);
    }

    @Override
    protected void inchLeft(float velocity, int duration) {
        move = true;
        while (move) {
            getmBase().setAngularVelocity(-velocity);
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    move = false;
                }
            }, duration);
        }
        getmBase().setAngularVelocity(0);
    }

    @Override
    protected void shiver() {
        shiverCount = 0;
        final Handler handler = new Handler(Looper.getMainLooper());
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (even) {
                    inchLeft(getVelocity() * 16, getDuration() / 16);
                } else {
                    inchRight(getVelocity() * 16, getDuration() / 16);
                }
                even = !even;
                shiverCount++;
                if (shiverCount < 16)
                    handler.postDelayed(this, getDuration() / 16);
            }
        };
        handler.postDelayed(runnable, 0);
    }
}
