package de.maxfeigl.loomo.bodyParts;

import android.view.View;

import de.maxfeigl.emotions.bodyparts.Eye;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 17.01.2018.
 */

public class MyEye extends Eye {
    private View leftEyeView;
    private View rightEyeView;

    public MyEye(View leftEyeView, View rightEyeView, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super(leftEyeView, rightEyeView, jsonConverter, mandatoryCheck);
    }

    @Override
    protected void moveLeft(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().x(leftEyeView.getX() - getEyeMovementInPx()).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().x(rightEyeView.getX() - getEyeMovementInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void moveRight(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().x(leftEyeView.getX() + getEyeMovementInPx()).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().x(rightEyeView.getX() + getEyeMovementInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void moveUp(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().y(leftEyeView.getY() - getEyeMovementInPx()).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().y(rightEyeView.getY() - getEyeMovementInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void moveDown(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().y(leftEyeView.getY() + getEyeMovementInPx()).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().y(rightEyeView.getY() + getEyeMovementInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void close(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().scaleY(.1f).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().scaleY(.1f).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void open(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().scaleY(1.25f).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().scaleY(1.25f).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void defaultLook(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().scaleY(1f).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().scaleY(1f).setDuration(super.getMovementSpeed());
    }
}
