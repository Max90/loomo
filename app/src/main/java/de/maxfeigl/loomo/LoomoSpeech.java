package de.maxfeigl.loomo;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;

import com.segway.robot.sdk.base.bind.ServiceBinder;
import com.segway.robot.sdk.baseconnectivity.Message;
import com.segway.robot.sdk.emoji.behavior.Sections;
import com.segway.robot.sdk.emoji.exception.EmojiException;
import com.segway.robot.sdk.emoji.player.RobotAnimator;
import com.segway.robot.sdk.voice.Speaker;
import com.segway.robot.sdk.voice.VoiceException;
import com.segway.robot.sdk.voice.tts.TtsListener;

import static android.graphics.PorterDuff.Mode.CLEAR;

/**
 * Created by Max on 14.12.2017.
 */

public class LoomoSpeech {
    private Speaker mSpeaker;

    private ServiceBinder.BindStateListener mSpeakerBindStateListener = new ServiceBinder.BindStateListener() {
        @Override
        public void onBind() {

        }

        @Override
        public void onUnbind(String reason) {

        }
    };

    private TtsListener mTtsListener = new TtsListener() {
        @Override
        public void onSpeechStarted(String s) {
            //s is speech content, callback this method when speech is starting.

        }

        @Override
        public void onSpeechFinished(String s) {
            //s is speech content, callback this method when speech is finish.

        }

        @Override
        public void onSpeechError(String s, String s1) {
            //s is speech content, callback this method when speech occurs error.
        }
    };

    public LoomoSpeech(Context context) {
        mSpeaker = Speaker.getInstance();
        //bind the speaker service.
        mSpeaker.bindService(context, mSpeakerBindStateListener);
        int resID = context.getResources().getIdentifier("beep", "raw", context.getPackageName());

        MediaPlayer mediaPlayer = MediaPlayer.create(context, resID);
        mediaPlayer.start();
    }
}
