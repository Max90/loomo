package de.maxfeigl.emotions.bodyparts;

import android.util.Log;
import android.view.View;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 05.01.2018.
 */

public abstract class Eyelid extends BodyPart {
    private View leftEyeView;
    private View rightEyeView;

    public Eyelid(View leftEyeView, View rightEyeView, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super();
        this.leftEyeView = leftEyeView;
        this.rightEyeView = rightEyeView;
        this.mandatoryCheck = mandatoryCheck;
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
    }

    protected abstract void lowerEyelid(View leftEyeView, View rightEyeView);

    protected abstract void raiseEyelid(View leftEyeView, View rightEyeView);

    protected abstract void lowerLowerEyelid(View leftEyeView, View rightEyeView);

    protected abstract void defaultEyelidPosition(View leftEyeView, View rightEyeView);

    protected abstract void closeEyelid(View leftEyeView, View rightEyeView);

    @Override
    protected void executeFunctionForName(String name) {
        Log.i("movements", name);
        switch (name) {
            case "Augenlider hängen herab":
                lowerEyelid(leftEyeView, rightEyeView);
                break;
            case "Augenlider geschlossen":
                closeEyelid(leftEyeView, rightEyeView);
                break;
            case "oberes Augenlid erhöht":
                raiseEyelid(leftEyeView, rightEyeView);
                break;
            case "unteres augenlid nach unten gezogen":
                lowerLowerEyelid(leftEyeView, rightEyeView);
                break;
            default:
                mandatoryCheck.setMandatoryFailed(true);
                break;

        }
        super.addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(name);
    }

    @Override
    protected void executeReverseFunctionForName(String name) {
        Log.i("movements", "reverse " + name);
        switch (name) {
            case "Augenlider hängen herab":
                defaultEyelidPosition(leftEyeView, rightEyeView);
                break;
            case "Augenlider geschlossen":
                defaultEyelidPosition(leftEyeView, rightEyeView);
                break;
            case "oberes Augenlid erhöht":
                defaultEyelidPosition(leftEyeView, rightEyeView);
                break;
            case "unteres augenlid nach unten gezogen":
                defaultEyelidPosition(leftEyeView, rightEyeView);
                break;
        }
    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {
        super.setBodyPart(object);
    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        switch (name) {
            case "Augenlider hängen herab":
                return true;
            case "Augenlider geschlossen":
                return true;
            case "oberes Augenlid erhöht":
                return true;
            case "unteres augenlid nach unten gezogen":
                return true;
            default:
                if (checkForFallback) {
                    super.checkIfFallbackPossible(name);
                } else {
                    return false;
                }
                break;
        }
        return true;
    }
}
