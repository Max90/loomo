package de.maxfeigl.emotions.bodyparts;

import android.util.Log;
import android.view.View;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 13.12.2017.
 */

public abstract class Eye extends BodyPart {
    private static final int EYE_MOVEMENT_IN_PX = 20;
    private View leftEyeView;
    private View rightEyeView;

    public Eye(View leftEyeView, View rightEyeView, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super();
        this.leftEyeView = leftEyeView;
        this.rightEyeView = rightEyeView;
        this.mandatoryCheck = mandatoryCheck;
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
    }

    public static int getEyeMovementInPx() {
        return EYE_MOVEMENT_IN_PX;
    }

    protected abstract void moveLeft(View leftEyeView, View rightEyeView);

    protected abstract void moveRight(View leftEyeView, View rightEyeView);

    protected abstract void moveUp(View leftEyeView, View rightEyeView);

    protected abstract void moveDown(View leftEyeView, View rightEyeView);

    protected abstract void close(View leftEyeView, View rightEyeView);

    protected abstract void open(View leftEyeView, View rightEyeView);

    protected abstract void defaultLook(View leftEyeView, View rightEyeView);

    @Override
    protected void executeFunctionForName(String name) {
        Log.i("movements", name);
        switch (name) {
            case "Augen wandern rechts":
                moveRight(leftEyeView, rightEyeView);
                break;
            case "Augen wandern links":
                moveLeft(leftEyeView, rightEyeView);
                break;
            case "Augen wandern oben":
                moveUp(leftEyeView, rightEyeView);
                break;
            case "Augen wandern unten":
                moveDown(leftEyeView, rightEyeView);
                break;
            case "Augen geöffnet":
                open(leftEyeView, rightEyeView);
                break;
            case "Augen geschlossen":
                close(leftEyeView, rightEyeView);
                break;
            default:
                mandatoryCheck.setMandatoryFailed(true);
                break;
        }
        super.addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(name);
    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        switch (name) {
            case "Augen wandern rechts":
                break;
            case "Augen wandern links":
                break;
            case "Augen wandern oben":
                break;
            case "Augen wandern unten":
                break;
            case "Augen geöffnet":
                break;
            case "Augen geschlossen":
                break;
            default:
                if (checkForFallback)
                    super.checkIfFallbackPossible(name);
                else
                    return false;
                break;
        }
        return true;
    }

    @Override
    protected void executeReverseFunctionForName(String name) {
        Log.i("movements", "reverse " + name);
        switch (name) {
            case "Augen wandern rechts":
                moveLeft(leftEyeView, rightEyeView);
                break;
            case "Augen wandern links":
                moveRight(leftEyeView, rightEyeView);
                break;
            case "Augen wandern oben":
                moveDown(leftEyeView, rightEyeView);
                break;
            case "Augen wandern unten":
                moveUp(leftEyeView, rightEyeView);
                break;
            case "Augen geöffnet":
                defaultLook(leftEyeView, rightEyeView);
                break;
            case "Augen geschlossen":
                defaultLook(leftEyeView, rightEyeView);
                break;
        }
    }

    @Override
    public void setConcreteBodyPart(JSONObject jsonObject) {
        super.setBodyPart(jsonObject);
    }
}
