package de.maxfeigl.emotions.helpers;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by Max on 11.01.2018.
 */

public class MandatoryCheck extends ClassLoader {
    JSONArray mandatoryBodyParts;
    private boolean mandatoryFailed = false;
    private ArrayList<String> fallbackMovements = new ArrayList<>();

    public MandatoryCheck(JSONConverter jsonConverter) {
        mandatoryBodyParts = new JSONArray();
        try {
            mandatoryBodyParts = jsonConverter.getMandatoryBodyParts();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void checkIfMandatoryBodyPartsAreInitialized() {
        for (int i = 0; i < mandatoryBodyParts.length(); i++) {
            String name = null;
            try {
                name = mandatoryBodyParts.get(i).toString();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            switch (name) {
                case "Arme":
                    checkIfClassIsLoaded(name);
                    break;
                case "Augen":
                    checkIfClassIsLoaded(name);
                    break;
                case "Augenbrauen":
                    checkIfClassIsLoaded(name);
                    break;
                case "Lider":
                    checkIfClassIsLoaded(name);
                    break;
                case "Gesicht":
                    checkIfClassIsLoaded(name);
                    break;
                case "Lippen":
                    checkIfClassIsLoaded(name);
                    break;
                case "Mund":
                    checkIfClassIsLoaded(name);
                    break;
                case "Kopf":
                    checkIfClassIsLoaded(name);
                    break;
                case "Stimme":
                    checkIfClassIsLoaded(name);
                    break;
                case "Oberkörper":
                    checkIfClassIsLoaded(name);
                    break;
            }
        }
    }

    private void checkIfClassIsLoaded(String className) {
        if (findLoadedClass(className) == null) {
            mandatoryFailed = true;
        }
    }

    public boolean isMandatoryFailed() {
        return mandatoryFailed;
    }

    public void setMandatoryFailed(boolean mandatoryFailed) {
        this.mandatoryFailed = mandatoryFailed;
    }

    public ArrayList<String> getFallbackMovements() {
        return fallbackMovements;
    }

    public void addToFallbackMovements(String movementName) {
        this.fallbackMovements.add(movementName);
    }

    public void removeFromFallbackMovements(int i) {
        fallbackMovements.remove(i);
    }
}
