package de.maxfeigl.emotions.bodyparts;

import android.content.Context;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.JSONConverter;

/**
 * Created by Max on 11.01.2018.
 */

public class Arms extends BodyPart {
    private Context context;

    public Arms(Context context, JSONConverter jsonConverter) {
        super();
        this.context = context;
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
    }

    @Override
    protected void executeFunctionForName(String name) {

    }

    @Override
    protected void executeReverseFunctionForName(String name) {

    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {

    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        return true;
    }
}
