package de.maxfeigl.emotions.bodyparts;

import android.content.Context;

import org.json.JSONObject;

import de.maxfeigl.emotions.helpers.JSONConverter;

/**
 * Created by Max on 21.11.2017.
 */

public class Face extends BodyPart {
    private JSONConverter jsonConverter;
    private JSONObject faceJSON;

    public Face(Context context, JSONConverter jsonConverter) {
        super();
        this.jsonConverter = jsonConverter;
    }

    public JSONObject getFaceJSON() {
        return faceJSON;
    }

    public void setFaceJSON(JSONObject faceJSON) {
        this.faceJSON = faceJSON;
    }

    @Override
    protected void executeFunctionForName(String name) {

    }

    @Override
    protected void executeReverseFunctionForName(String name) {

    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {

    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        return true;
    }
}
