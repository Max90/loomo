package de.maxfeigl.loomo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EmotionSelectionActivity extends Activity implements View.OnClickListener {

    private Button btFear;
    private Button btAnxiety;
    private Button btDisgust;
    private Button btHappiness;
    private Button btSadness;
    private Button btAnger;
    private Button btSurprise;
    private Button btUncertainty;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emotion_selection);

        btFear = findViewById(R.id.bt_fear);
        btAnxiety = findViewById(R.id.bt_anxiety);
        btDisgust = findViewById(R.id.bt_disgust);
        btHappiness = findViewById(R.id.bt_happiness);
        btSadness = findViewById(R.id.bt_sadness);
        btAnger = findViewById(R.id.bt_anger);
        btSurprise = findViewById(R.id.bt_surprise);
        btUncertainty = findViewById(R.id.bt_uncertainty);

        btFear.setOnClickListener(this);
        btAnxiety.setOnClickListener(this);
        btDisgust.setOnClickListener(this);
        btHappiness.setOnClickListener(this);
        btSadness.setOnClickListener(this);
        btAnger.setOnClickListener(this);
        btSurprise.setOnClickListener(this);
        btUncertainty.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.bt_anger:
                i = new Intent(this, MainActivity.class);
                i.putExtra("emotion", "wut");
                startActivity(i);
                break;
            case R.id.bt_fear:
                i = new Intent(this, MainActivity.class);
                i.putExtra("emotion", "angst");
                startActivity(i);
                break;
            case R.id.bt_anxiety:
                i = new Intent(this, MainActivity.class);
                i.putExtra("emotion", "beklemmung");
                startActivity(i);
                break;
            case R.id.bt_disgust:
                i = new Intent(this, MainActivity.class);
                i.putExtra("emotion", "ekel");
                startActivity(i);
                break;
            case R.id.bt_happiness:
                i = new Intent(this, MainActivity.class);
                i.putExtra("emotion", "glueck");
                startActivity(i);
                break;
            case R.id.bt_sadness:
                i = new Intent(this, MainActivity.class);
                i.putExtra("emotion", "traurigkeit");
                startActivity(i);
                break;
            case R.id.bt_surprise:
                i = new Intent(this, MainActivity.class);
                i.putExtra("emotion", "ueberraschung");
                startActivity(i);
                break;
            case R.id.bt_uncertainty:
                i = new Intent(this, MainActivity.class);
                i.putExtra("emotion", "unsicherheit");
                startActivity(i);
                break;
        }
    }
}
