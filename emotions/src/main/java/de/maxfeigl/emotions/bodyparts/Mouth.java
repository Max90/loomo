package de.maxfeigl.emotions.bodyparts;

import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 22.12.2017.
 */

public abstract class Mouth extends BodyPart {

    private ImageView mouth;
    private Context context;

    public Mouth(ImageView mouth, Context context, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super();
        this.mouth = mouth;
        this.context = context;
        this.mandatoryCheck = mandatoryCheck;
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
    }

    protected abstract void jawDrop();

    protected abstract void jawDropReverse();

    protected abstract void openMouth();

    protected void executeMovement(int drawable) {
        mouth.setImageDrawable(context.getDrawable(drawable));
        Drawable d = mouth.getDrawable();
        ((AnimatedVectorDrawable) d).start();
    }


    @Override
    protected void executeFunctionForName(String name) {
        Log.i("movements", name);
        switch (name) {
            case "Mund geöffnet":
                openMouth();
                break;
            case "Kinn fällt herunter":
                jawDrop();
                break;
        }
        super.addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(name);
    }

    @Override
    protected void executeReverseFunctionForName(String name) {
        switch (name) {
            case "Mund geöffnet":
                openMouthReverse();
                break;
            case "Kinn fällt herunter":
                jawDropReverse();
                break;
        }
    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {
        super.setBodyPart(object);
    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        switch (name) {
            case "Mund geöffnet":
                break;
            case "Kinn fällt herunter":
                break;
            default:
                if (checkForFallback)
                    super.checkIfFallbackPossible(name);
                else
                    return false;
                break;
        }
        return true;
    }

    protected abstract void openMouthReverse();
}
