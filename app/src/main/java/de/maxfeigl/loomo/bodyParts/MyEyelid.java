package de.maxfeigl.loomo.bodyParts;

import android.view.View;

import de.maxfeigl.emotions.bodyparts.Eyelid;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 17.01.2018.
 */

public class MyEyelid extends Eyelid {
    public MyEyelid(View leftEyeView, View rightEyeView, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super(leftEyeView, rightEyeView, jsonConverter, mandatoryCheck);
    }

    @Override
    protected void lowerEyelid(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null) {
            leftEyeView.setPivotY(0);
            leftEyeView.animate().scaleY(.5f).setDuration(super.getMovementSpeed());
        }
        if (rightEyeView != null) {
            rightEyeView.setPivotY(0);
            rightEyeView.animate().scaleY(.5f).setDuration(super.getMovementSpeed());
        }
    }

    @Override
    protected void raiseEyelid(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null) {
            leftEyeView.setPivotY(rightEyeView.getHeight());
            leftEyeView.animate().scaleY(1.5f).setDuration(super.getMovementSpeed());
        }
        if (rightEyeView != null) {
            rightEyeView.setPivotY(rightEyeView.getHeight());
            rightEyeView.animate().scaleY(1.5f).setDuration(super.getMovementSpeed());
        }
    }

    @Override
    protected void lowerLowerEyelid(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null) {
            leftEyeView.setPivotY(0);
            leftEyeView.animate().scaleY(1.5f).setDuration(super.getMovementSpeed());
        }
        if (rightEyeView != null) {
            rightEyeView.setPivotY(0);
            rightEyeView.animate().scaleY(1.5f).setDuration(super.getMovementSpeed());
        }
    }

    @Override
    protected void defaultEyelidPosition(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().scaleY(1f).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().scaleY(1f).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void closeEyelid(View leftEyeView, View rightEyeView) {
        if (leftEyeView != null)
            leftEyeView.animate().scaleY(.1f).setDuration(super.getMovementSpeed());
        if (rightEyeView != null)
            rightEyeView.animate().scaleY(.1f).setDuration(super.getMovementSpeed());
    }
}
