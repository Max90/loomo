package de.maxfeigl.emotions.bodyparts;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.segway.robot.sdk.base.bind.ServiceBinder;
import com.segway.robot.sdk.locomotion.sbv.Base;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.AppConstants;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 12.12.2017.
 */

public abstract class Wheels extends BodyPart {
    private Base mBase;
    private Context context;
    private boolean bindSuccess = false;
    private float velocity;
    private int duration;

    public Wheels(Context context, JSONConverter jsonConverter) {
        super();
        this.context = context;
        reinitializeWheels(jsonConverter, null);
    }

    protected abstract void moveForward(float velocity, int duration);

    protected abstract void moveBackwards(float velocity, int duration);

    protected abstract void turnLeft(float velocity);

    protected abstract void turnRight(float velocity);

    protected abstract void inchRight(float velocity, int duration);

    protected abstract void inchLeft(float velocity, int duration);

    protected abstract void shiver();

    public void setSpeeds() {
        setMovementSpeed();
        setMovementDuration();
    }

    private void setMovementDuration() {
        if (duration == AppConstants.MANY_MOVEMENTS)
            duration = AppConstants.MANY_MOVEMENTS / 3;
        else
            duration = AppConstants.FEW_MOVEMENTS / 3;
    }

    private void setMovementSpeed() {
        if (movementSpeed == AppConstants.FAST_MOVEMENT_SPEED)
            velocity = 3;
        else
            velocity = 0.25f;
    }

    /**
     * resets all arrayLists back to empty lists
     * needs to be different than other body parts because bindService may fail otherwise
     *
     * @param jsonConverter
     */
    public void reinitializeWheels(JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
        this.mandatoryCheck = mandatoryCheck;
    }

    public void bindWheelsService() {
        mBase = Base.getInstance();
        bindService(mBase);
    }

    private void bindService(final Base mBase) {
        mBase.bindService(context.getApplicationContext(), new ServiceBinder.BindStateListener() {
            @Override
            public void onBind() {
                Log.i("Loomo", "service bind");
                bindSuccess = true;
                mBase.setControlMode(Base.CONTROL_MODE_RAW);
            }

            @Override
            public void onUnbind(String reason) {
            }
        });
    }

    @Override
    protected void executeFunctionForName(String name) {
        Log.i("movements", name);
        switch (name) {
            case "nach links drehen":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        turnLeft(velocity);
                    }
                });
                break;
            case "nach rechts drehen":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        turnRight(velocity);
                    }
                });
                break;
            case "nach vorn fahren":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        moveForward(velocity, duration);
                    }
                });
                break;
            case "nach hinten fahren":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        moveBackwards(velocity, duration);
                    }
                });
                break;
            case "Rückwärtsbewegung":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        moveBackwards(velocity, duration);
                    }
                });
                break;
            case "Richtungswechsel":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        moveBackwards(velocity, duration);
                    }
                });
                break;
            case "schaudern":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        shiver();
                    }
                });
                break;
            case "ruckartige Flucht":
                AsyncTask.execute(new Runnable() {
                    @Override
                    public void run() {
                        moveBackwards(velocity, duration / 2);
                    }
                });
                break;
            default:
                mandatoryCheck.setMandatoryFailed(true);
                break;
        }
        super.addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(name);
    }

    @Override
    protected void executeReverseFunctionForName(String name) {
        Log.i("movements", "reverse " + name);
        switch (name) {
            case "nach links drehen":
                turnRight(velocity);
                break;
            case "nach rechts drehen":
                turnLeft(velocity);
                break;
            case "nach vorn fahren":
                moveBackwards(velocity, duration / 2);
                break;
            case "nach hinten fahren":
                moveForward(velocity, duration / 2);
                break;
            case "Rückwärtsbewegung":
                moveForward(velocity, duration / 2);
                break;
            case "Richtungswechsel":
                moveForward(velocity, duration / 2);
                break;
            case "ruckartige Flucht":
                moveForward(velocity, duration / 2);
                break;
        }
    }

    public void setConcreteBodyPart(JSONObject object) {
        super.setBodyPart(object);
    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        switch (name) {
            case "nach links drehen":
                break;
            case "nach rechts drehen":
                break;
            case "nach vorn fahren":
                break;
            case "nach hinten fahren":
                break;
            case "Rückwärtsbewegung":
                break;
            case "Richtungswechsel":
                break;
            case "schaudern":
                break;
            case "ruckartige Flucht":
                break;
            default:
                if (checkForFallback)
                    super.checkIfFallbackPossible(name);
                else
                    return false;
                break;
        }
        return true;
    }

    public Base getmBase() {
        return mBase;
    }

    public Context getContext() {
        return context;
    }

    public boolean isBindSuccess() {
        return bindSuccess;
    }

    public float getVelocity() {
        return velocity;
    }

    public int getDuration() {
        return duration;
    }
}
