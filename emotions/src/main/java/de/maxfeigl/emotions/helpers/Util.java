package de.maxfeigl.emotions.helpers;

import android.content.res.Resources;
import android.util.DisplayMetrics;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

/**
 * Created by Max on 13.12.2017.
 */

public class Util {

    public static int getPhoneWidthInPixels() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int randomInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }

    public static Boolean sameSign(float x, float y) {
        return ((x < 0) == (y < 0));
    }

    public static float differenceBetweenAngles(float currentTheta, float targetTheta) {
        DecimalFormat numberFormat = new DecimalFormat("#.0000000");
        return Float.parseFloat(numberFormat.format(Math.abs(Math.abs(currentTheta) - Math.abs(targetTheta))));
    }

    public static JSONArray removeValueFromArray(JSONArray jsonArray, int position) throws JSONException {
        JSONArray list = new JSONArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            //Excluding the item at position
            if (i != position) {
                list.put(jsonArray.get(i));
            }
        }
        return list;
    }

    public static ArrayList<String> removeValueFromArrayList(ArrayList<String> arrayList, String emotionName) throws JSONException {
        ArrayList<String> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            //Excluding the item at position
            if (!arrayList.get(i).equals(emotionName)) {
                list.add(arrayList.get(i));
            }
        }
        return list;
    }

    public static int getIntForString(JSONArray jsonArray, String search) throws JSONException {
        for (int i = 0; i < jsonArray.length(); i++) {
            if (jsonArray.get(i).equals(search))
                return i;
        }
        return -1;
    }

    public final int convertPxToDp(int px) {
        return Math.round(px / (Resources.getSystem().getDisplayMetrics().xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
