package de.maxfeigl.emotions.bodyparts;

import android.content.Context;
import android.graphics.drawable.AnimatedVectorDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.ImageView;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 05.01.2018.
 */

public abstract class Lip extends BodyPart {
    private ImageView lip;
    private Context context;

    public Lip(ImageView mouth, Context context, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super();
        this.lip = mouth;
        this.context = context;
        this.mandatoryCheck = mandatoryCheck;
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
    }

    protected abstract void smile();

    protected abstract void smileReverse();

    protected abstract void lipsPressed();

    protected abstract void lipsPressedReverse();

    protected abstract void lipsDown();

    protected abstract void lipsDownReverse();

    protected abstract void contractLips();

    protected abstract void contractLipsReverse();

    protected abstract void lipsPressedAndUp();

    protected abstract void lipsPressedAndUpReverse();

    protected abstract void raiseLowerLip();

    protected abstract void raiseLowerLipReverse();

    protected abstract void lowerLowerLip();

    protected abstract void lowerLowerLipReverse();

    protected abstract void raiseUpperLip();

    protected abstract void raiseUpperLipReverse();

    protected abstract void raiseUpperAndLowerLip();

    public void executeMovement(int drawable) {
        lip.setImageDrawable(context.getDrawable(drawable));
        Drawable d = lip.getDrawable();
        ((AnimatedVectorDrawable) d).start();
    }

    @Override
    protected void executeFunctionForName(String name) {
        Log.i("movements", name);
        switch (name) {
            case "Lippen Ecken nach oben gezogen":
                smile();
                break;
            case "Lippen Ecken nach hinten gezogen":
                smile();
                break;
            case "Lippen Ecken nach unten gezogen":
                lipsDown();
                break;
            case "Lippen zusammengepresst":
                lipsPressed();
                break;
            case "Lippen zusammengezogen":
                contractLips();
                break;
            case "Lippen zusammengepresst u Ecken nach oben gezogen":
                lipsPressedAndUp();
                break;
            case "Lippen Ecken nach hinten gezogen u Lippen Ecken nach oben gezogen":
                smileReverse();
                break;
            case "Unterlippe erhöht":
                raiseLowerLip();
                break;
            case "Unterlippe gesenkt":
                lowerLowerLip();
                break;
            case "Unterlippe zur Oberlippe gedrückt":
                lipsPressed();
                break;
            case "Oberlippe erhöht":
                raiseUpperLip();
                break;
            case "Oberlippe erhöht u Unterlippe zur Überlippe gedrückt":
                raiseUpperAndLowerLip();
                break;
            default:
                mandatoryCheck.setMandatoryFailed(true);
                break;
        }
        super.addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(name);
    }

    @Override
    protected void executeReverseFunctionForName(String name) {
        Log.i("movements", "reverse " + name);
        switch (name) {
            case "Lippen Ecken nach oben gezogen":
                smileReverse();
                break;
            case "Lippen Ecken nach hinten gezogen":
                smileReverse();
                break;
            case "Lippen Ecken nach unten gezogen":
                lipsDownReverse();
                break;
            case "Lippen zusammengepresst":
                lipsPressedReverse();
                break;
            case "Lippen zusammengezogen":
                contractLipsReverse();
                break;
            case "Lippen zusammengepresst u Ecken nach oben gezogen":
                lipsPressedAndUpReverse();
                break;
            case "Lippen Ecken nach hinten gezogen u Lippen Ecken nach oben gezogen":
                smile();
                break;
            case "Unterlippe erhöht":
                raiseLowerLipReverse();
                break;
            case "Unterlippe gesenkt":
                lowerLowerLipReverse();
                break;
            case "Unterlippe zur Oberlippe gedrückt":
                lipsPressedReverse();
                break;
            case "Oberlippe erhöht":
                raiseUpperLipReverse();
                break;
            case "Oberlippe erhöht u Unterlippe zur Überlippe gedrückt":
                raiseUpperAndLowerLipReverse();
                break;
        }
    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {
        super.setBodyPart(object);
    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        switch (name) {
            case "Lippen Ecken nach oben gezogen":
                break;
            case "Lippen Ecken nach unten gezogen":
                break;
            case "Lippen Ecken nach hinten gezogen":
                break;
            case "Lippen zusammengepresst":
                break;
            case "Lippen zusammengezogen":
                break;
            case "Lippen zusammengepresst u Ecken nach oben gezogen":
                break;
            case "Lippen Ecken nach hinten gezogen u Lippen Ecken nach oben gezogen":
                break;
            case "Unterlippe erhöht":
                break;
            case "Unterlippe gesenkt":
                break;
            case "Unterlippe zur Oberlippe gedrückt":
                break;
            case "Oberlippe erhöht":
                break;
            case "Oberlippe erhöht u Unterlippe zur Überlippe gedrückt":
                break;
            default:
                if (checkForFallback)
                    super.checkIfFallbackPossible(name);
                else
                    return false;
                break;
        }
        return true;
    }

    protected ImageView getLip() {
        return lip;
    }

    public Context getContext() {
        return context;
    }

    protected abstract void raiseUpperAndLowerLipReverse();
}

