package de.maxfeigl.emotions;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import de.maxfeigl.emotions.bodyparts.Eye;
import de.maxfeigl.emotions.bodyparts.Eyebrow;
import de.maxfeigl.emotions.bodyparts.Eyelid;
import de.maxfeigl.emotions.bodyparts.Lip;
import de.maxfeigl.emotions.bodyparts.LoomoHead;
import de.maxfeigl.emotions.bodyparts.Mouth;
import de.maxfeigl.emotions.bodyparts.Movement;
import de.maxfeigl.emotions.bodyparts.Voice;
import de.maxfeigl.emotions.bodyparts.Wheels;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 17.01.2018.
 */

public class EmotionExecutor {
    private Context context;
    private Eye eyes;
    private Eyebrow eyebrows;
    private Mouth mouth;
    private Lip lip;
    private Eyelid eyelid;
    private Voice voice;
    private Movement movement;
    private MandatoryCheck mandatoryCheck;
    private JSONConverter jsonConverter;
    private Wheels wheels;
    private LoomoHead head;
    private boolean movementPresent;
    private boolean eyebrowsPresent;
    private boolean eyesPresent;
    private boolean mouthPresent;
    private boolean lipPresent;
    private boolean eyelidPresent;
    private boolean voicePresent;
    private boolean wheelsPresent;
    private boolean headPresent;

    public EmotionExecutor(Eye eyes, Eyebrow eyebrows, Mouth mouth, Lip lip, Eyelid eyelid, Voice voice,
                           Movement movement, MandatoryCheck mandatoryCheck, JSONConverter jsonConverter,
                           Wheels wheels, LoomoHead head, Context context) {
        this.eyes = eyes;
        this.eyebrows = eyebrows;
        this.mouth = mouth;
        this.lip = lip;
        this.eyelid = eyelid;
        this.voice = voice;
        this.movement = movement;
        this.mandatoryCheck = mandatoryCheck;
        this.jsonConverter = jsonConverter;
        this.wheels = wheels;
        this.head = head;
        this.context = context;
    }

    public void executeInitialMovements() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                checkIfMandatoryMovementsPossible();

                // if a mandatory movement is not possible do not display the emotion
                if (!mandatoryCheck.isMandatoryFailed() && mandatoryCheck.getFallbackMovements().size() == 0) {
                    executeMovements();
                } else {
                    Toast.makeText(context, R.string.mandatory_failed_toast, Toast.LENGTH_LONG).show();
                }
            }
        }, 0);
    }

    public void reverseMovements() {
        new Handler().postDelayed(new Runnable() {
            public void run() {
                // execute a random movement for each body part if its initialization was successful
                if (eyebrowsPresent) {
                    eyebrows.reverseExecutedMovements();
                }
                if (eyesPresent) {
                    eyes.reverseExecutedMovements();
                }
                if (wheelsPresent) {
                    wheels.reverseExecutedMovements();
                }
                if (mouthPresent) {
                    mouth.reverseExecutedMovements();
                }
                if (lipPresent) {
                    lip.reverseExecutedMovements();
                }
                if (eyelidPresent) {
                    eyelid.reverseExecutedMovements();
                }
                if (headPresent) {
                    head.reverseExecutedMovements();
                }
                if (voicePresent) {
                    voice.reverseExecutedMovements();
                }
            }
        }, movement.getDelayBetweenMovements() + movement.getMovementSpeed());
    }

    /**
     * checks if all mandatory movements are possible
     * if one movement is not possible set flag
     */
    private void checkIfMandatoryMovementsPossible() {
        if (eyebrowsPresent) {
            eyebrows.checkMandatoryMovements();
        }
        if (eyesPresent) {
            eyes.checkMandatoryMovements();
        }
        if (wheelsPresent) {
            wheels.checkMandatoryMovements();
        }
        if (mouthPresent) {
            mouth.checkMandatoryMovements();
        }
        if (lipPresent) {
            lip.checkMandatoryMovements();
        }
        if (eyelidPresent) {
            eyelid.checkMandatoryMovements();
        }
        if (headPresent) {
            head.checkMandatoryMovements();
        }
        if (voicePresent) {
            voice.checkMandatoryMovements();
        }

        if (eyebrowsPresent) {
            eyebrows.checkIfFallbackExecuatable();
        }
        if (eyesPresent) {
            eyes.checkIfFallbackExecuatable();
        }
        if (wheelsPresent) {
            wheels.checkIfFallbackExecuatable();
        }
        if (mouthPresent) {
            mouth.checkIfFallbackExecuatable();
        }
        if (lipPresent) {
            lip.checkIfFallbackExecuatable();
        }
        if (eyelidPresent) {
            eyelid.checkIfFallbackExecuatable();
        }
        if (headPresent) {
            head.checkIfFallbackExecuatable();
        }
        if (voicePresent) {
            voice.checkIfFallbackExecuatable();
        }
    }

    private void executeMovements() {
        if (eyebrowsPresent) {
            eyebrows.executeMandatoryInstructions();
            eyebrows.executeRandomMovement();
        }
        if (eyesPresent) {
            eyes.executeMandatoryInstructions();
            eyes.executeRandomMovement();
        }
        if (wheelsPresent) {
            wheels.executeMandatoryInstructions();
            wheels.executeRandomMovement();
        }
        if (mouthPresent) {
            mouth.executeMandatoryInstructions();
            mouth.executeRandomMovement();
        }
        if (lipPresent) {
            lip.executeMandatoryInstructions();
            lip.executeRandomMovement();

        }
        if (eyelidPresent) {
            eyelid.executeMandatoryInstructions();
            eyelid.executeRandomMovement();
        }
        if (headPresent) {
            head.executeMandatoryInstructions();
            head.executeRandomMovement();
        }
        if (voicePresent) {
            voice.executeMandatoryInstructions();
            voice.executeRandomMovement();
        }
    }

    public void setMovementPresent(boolean movementPresent) {
        this.movementPresent = movementPresent;
    }

    public void setEyebrowsPresent(boolean eyebrowsPresent) {
        this.eyebrowsPresent = eyebrowsPresent;
    }

    public void setEyesPresent(boolean eyesPresent) {
        this.eyesPresent = eyesPresent;
    }

    public void setMouthPresent(boolean mouthPresent) {
        this.mouthPresent = mouthPresent;
    }

    public void setLipPresent(boolean lipPresent) {
        this.lipPresent = lipPresent;
    }

    public void setEyelidPresent(boolean eyelidPresent) {
        this.eyelidPresent = eyelidPresent;
    }

    public void setVoicePresent(boolean voicePresent) {
        this.voicePresent = voicePresent;
    }

    public void setWheelsPresent(boolean wheelsPresent) {
        this.wheelsPresent = wheelsPresent;
    }

    public void setHeadPresent(boolean headPresent) {
        this.headPresent = headPresent;
    }

    public void setEyes(Eye eyes) {
        this.eyes = eyes;
    }

    public void setEyebrows(Eyebrow eyebrows) {
        this.eyebrows = eyebrows;
    }

    public void setMouth(Mouth mouth) {
        this.mouth = mouth;
    }

    public void setLip(Lip lip) {
        this.lip = lip;
    }

    public void setEyelid(Eyelid eyelid) {
        this.eyelid = eyelid;
    }

    public void setVoice(Voice voice) {
        this.voice = voice;
    }

    public void setMovement(Movement movement) {
        this.movement = movement;
    }

    public void setMandatoryCheck(MandatoryCheck mandatoryCheck) {
        this.mandatoryCheck = mandatoryCheck;
    }

    public void setJsonConverter(JSONConverter jsonConverter) {
        this.jsonConverter = jsonConverter;
    }

    public void setWheels(Wheels wheels) {
        this.wheels = wheels;
    }

    public void setHead(LoomoHead head) {
        this.head = head;
    }
}
