package de.maxfeigl.loomo.bodyParts;

import android.content.Context;
import android.widget.ImageView;

import de.maxfeigl.emotions.bodyparts.Lip;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;
import de.maxfeigl.loomo.R;

/**
 * Created by Max on 17.01.2018.
 */

public class MyLip extends Lip {
    public MyLip(ImageView mouth, Context context, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super(mouth, context, jsonConverter, mandatoryCheck);
    }

    @Override
    protected void smile() {
        executeMovement(R.drawable.avd_smile);
    }

    @Override
    protected void smileReverse() {
        executeMovement(R.drawable.avd_smile_reverse);
    }

    @Override
    protected void lipsPressed() {
        executeMovement(R.drawable.avd_lips_pressed);
    }

    @Override
    protected void lipsPressedReverse() {
        executeMovement(R.drawable.avd_lips_pressed_reverse);
    }

    @Override
    protected void lipsDown() {
        executeMovement(R.drawable.avd_lips_down);
    }

    @Override
    protected void lipsDownReverse() {
        executeMovement(R.drawable.avd_lips_down_reverse);
    }

    @Override
    protected void contractLips() {
        executeMovement(R.drawable.avd_lips_contracted);
    }

    @Override
    protected void contractLipsReverse() {
        executeMovement(R.drawable.avd_lips_contracted_reverse);
    }

    @Override
    protected void lipsPressedAndUp() {
        executeMovement(R.drawable.avd_lips_pressed_up);
    }

    @Override
    protected void lipsPressedAndUpReverse() {
        executeMovement(R.drawable.avd_lips_pressed_up_reverse);
    }

    @Override
    protected void raiseLowerLip() {
        executeMovement(R.drawable.avd_raise_lower_lip);
    }

    @Override
    protected void raiseLowerLipReverse() {
        executeMovement(R.drawable.avd_raise_lower_lip_reverse);
    }

    @Override
    protected void lowerLowerLip() {
        executeMovement(R.drawable.avd_lower_lip_lowered);
    }

    @Override
    protected void lowerLowerLipReverse() {
        executeMovement(R.drawable.avd_lower_lip_lowered_reverse);
    }

    @Override
    protected void raiseUpperLip() {
        executeMovement(R.drawable.avd_upper_lip_raised);
    }

    @Override
    protected void raiseUpperLipReverse() {
        executeMovement(R.drawable.avd_upper_lip_raised_reverse);
    }

    @Override
    protected void raiseUpperAndLowerLip() {
        executeMovement(R.drawable.avd_disgust);
        getLip().animate().y(getLip().getY() - 20).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void raiseUpperAndLowerLipReverse() {
        super.executeMovement(R.drawable.avd_disgust_reverse);
        getLip().animate().y(getLip().getY() + 20).setDuration(super.getMovementSpeed());
    }
}
