package de.maxfeigl.loomo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import org.json.JSONException;

import java.io.IOException;

import de.maxfeigl.emotions.EmotionExecutor;
import de.maxfeigl.emotions.bodyparts.Eye;
import de.maxfeigl.emotions.bodyparts.Eyebrow;
import de.maxfeigl.emotions.bodyparts.Eyelid;
import de.maxfeigl.emotions.bodyparts.Lip;
import de.maxfeigl.emotions.bodyparts.LoomoHead;
import de.maxfeigl.emotions.bodyparts.Mouth;
import de.maxfeigl.emotions.bodyparts.Movement;
import de.maxfeigl.emotions.bodyparts.Voice;
import de.maxfeigl.emotions.bodyparts.Wheels;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;
import de.maxfeigl.loomo.bodyParts.MyEye;
import de.maxfeigl.loomo.bodyParts.MyEyebrow;
import de.maxfeigl.loomo.bodyParts.MyEyelid;
import de.maxfeigl.loomo.bodyParts.MyHead;
import de.maxfeigl.loomo.bodyParts.MyLip;
import de.maxfeigl.loomo.bodyParts.MyMouth;
import de.maxfeigl.loomo.bodyParts.MyVoice;
import de.maxfeigl.loomo.bodyParts.MyWheels;


public class MainActivity extends Activity {

    private View leftEyebrowView;
    private View rightEyebrowView;
    private View leftEyeView;
    private View rightEyeView;
    private ImageView mouthView;

    private JSONConverter jsonConverter;
    private MandatoryCheck mandatoryCheck;
    private Mouth mouth;
    private Lip lip;
    private Eyelid eyelid;
    private Movement movement;
    private Voice voice;
    private LoomoHead head;
    private Wheels wheels;
    private Eye eyes;
    private Eyebrow eyebrows;

    private boolean eyesPresent;
    private boolean eyebrowsPresent;
    private boolean wheelsPresent;
    private boolean mouthPresent;
    private boolean lipPresent;
    private boolean movementPresent;
    private boolean headPresent;
    private boolean eyelidPresent;
    private boolean voicePresent;

    private Handler handler;
    private EmotionExecutor emotionExecutor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        RelativeLayout viewContainer = findViewById(R.id.viewContainer);
        viewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getBaseContext(), EmotionSelectionActivity.class);
                startActivity(i);
            }
        });

        leftEyeView = findViewById(R.id.leftEye);
        rightEyeView = findViewById(R.id.rightEye);
        leftEyebrowView = findViewById(R.id.leftEyebrow);
        rightEyebrowView = findViewById(R.id.rightEyebrow);
        mouthView = findViewById(R.id.mouth);

        jsonConverter = new JSONConverter(this);
        initHead();
        initWheels();
    }

    @Override
    protected void onResume() {
        super.onResume();

        String emotionName = "modellbeispiel";
        Intent intent = getIntent();
        if (intent != null)
            if (intent.getStringExtra("emotion") != null)
                emotionName = intent.getStringExtra("emotion");

        Log.i("emotionName", emotionName);
        getModelFromJson(emotionName);
        mandatoryCheck = new MandatoryCheck(jsonConverter);
        initFace();
        initializeBodyParts();
    }

    /**
     * loads the json file for the selected emotion
     *
     * @param emotionName name of the selected emotion
     */
    private void getModelFromJson(String emotionName) {

        try {
            jsonConverter.getJSONfromFile(emotionName, emotionName);
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(this, "Datei konnte nicht gefunden werden", Toast.LENGTH_SHORT).show();
        } catch (JSONException e) {
            Toast.makeText(this, "Fehler beim Parsen der Datei", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void initFace() {
        try {
            jsonConverter.initializeFace();
        } catch (JSONException e) {
            Toast.makeText(this, "Fehler beim Initialisieren des Gesichts", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    private void initWheels() {
        wheels = new MyWheels(getBaseContext(), jsonConverter);
        wheels.bindWheelsService();
    }

    private void initHead() {
        head = new MyHead(getBaseContext(), jsonConverter);
        head.bindHeadService();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);

        //animations have to be executed in here because the layout is not fully crated in onCreate yet
        //which causes the getPostion() methods in the individual body part classes to return 0

        handler = new Handler();
        if (hasFocus) {

            handler.postDelayed(new Runnable() {
                public void run() {
                    mandatoryCheck.setMandatoryFailed(false);
                    Log.i("movements", "----------------------------------------");
                    initializeBodyParts();
                    if (!mandatoryCheck.isMandatoryFailed()) {
                        emotionExecutor.executeInitialMovements();

                        // reverse emotions only if mandatory check succeeded because otherwise no
                        // emotion was executed
                        if (!mandatoryCheck.isMandatoryFailed() && mandatoryCheck.getFallbackMovements().size() == 0) {
                            emotionExecutor.reverseMovements();
                            handler.postDelayed(this, movement.getDelayBetweenMovements() * 2 + movement.getMovementSpeed() * 2);
                        }
                    }
                }
            }, 0);
        }

    }

    private void initializeBodyParts() {
        movement = new Movement();
        try {
            movement.setMovement(jsonConverter.initializeMovement());
            movementPresent = true;
        } catch (JSONException e) {
            movementPresent = false;
            e.printStackTrace();
        }

        eyebrows = new MyEyebrow(leftEyebrowView, rightEyebrowView, getBaseContext(), jsonConverter, mandatoryCheck);
        try {
            eyebrows.setConcreteBodyPart(jsonConverter.initializeEyebrow());
            eyebrowsPresent = true;
        } catch (JSONException e) {
            eyebrowsPresent = false;
            e.printStackTrace();
        }

        eyes = new MyEye(leftEyeView, rightEyeView, jsonConverter, mandatoryCheck);
        try {
            eyes.setConcreteBodyPart(jsonConverter.initilizeEye());
            eyesPresent = true;
        } catch (JSONException e) {
            eyesPresent = false;
            e.printStackTrace();
        }

        wheels.reinitializeWheels(jsonConverter, mandatoryCheck);
        try {
            wheels.setConcreteBodyPart(jsonConverter.initializeWheels());
            wheels.setSpeeds();
            wheelsPresent = true;
        } catch (JSONException e) {
            wheelsPresent = false;
            e.printStackTrace();
        }

        head.reinitializeHead(jsonConverter, mandatoryCheck);
        try {
            head.setConcreteBodyPart(jsonConverter.initializeHead());
            head.setSpeeds();
            headPresent = true;
        } catch (JSONException e) {
            headPresent = false;
            e.printStackTrace();
        }

        mouth = new MyMouth(mouthView, getBaseContext(), jsonConverter, mandatoryCheck);
        try {
            mouth.setConcreteBodyPart(jsonConverter.initializeMouth());
            mouthPresent = true;
        } catch (JSONException e) {
            mouthPresent = false;
            e.printStackTrace();
        }

        lip = new MyLip(mouthView, getBaseContext(), jsonConverter, mandatoryCheck);
        try {
            lip.setConcreteBodyPart(jsonConverter.initializeLip());
            lipPresent = true;
        } catch (JSONException e) {
            lipPresent = false;
            e.printStackTrace();
        }

        eyelid = new MyEyelid(leftEyeView, rightEyeView, jsonConverter, mandatoryCheck);
        try {
            eyelid.setConcreteBodyPart(jsonConverter.initializeEyelid());
            eyelidPresent = true;
        } catch (JSONException e) {
            eyelidPresent = false;
            e.printStackTrace();
        }

        voice = new MyVoice(getBaseContext(), jsonConverter, mandatoryCheck);
        try {
            voice.setConcreteBodyPart(jsonConverter.initializeVoice());
            voicePresent = true;
        } catch (JSONException e) {
            voicePresent = false;
            e.printStackTrace();
        }

        mandatoryCheck.checkIfMandatoryBodyPartsAreInitialized();
        if (mandatoryCheck.isMandatoryFailed())
            Toast.makeText(this, R.string.mandatory_failed_toast, Toast.LENGTH_LONG).show();

        emotionExecutor = new EmotionExecutor(eyes, eyebrows, mouth, lip, eyelid, voice, movement,
                mandatoryCheck, jsonConverter, wheels, head, getBaseContext());
        emotionExecutor.setMovementPresent(movementPresent);
        emotionExecutor.setEyesPresent(eyesPresent);
        emotionExecutor.setEyebrowsPresent(eyebrowsPresent);
        emotionExecutor.setMouthPresent(mouthPresent);
        emotionExecutor.setLipPresent(lipPresent);
        emotionExecutor.setEyelidPresent(eyelidPresent);
        emotionExecutor.setVoicePresent(voicePresent);
        emotionExecutor.setWheelsPresent(wheelsPresent);
        emotionExecutor.setHeadPresent(headPresent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        handler.removeCallbacksAndMessages(null);
    }
}
