package de.maxfeigl.loomo.bodyParts;

import android.content.Context;
import android.widget.ImageView;

import de.maxfeigl.emotions.bodyparts.Mouth;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;
import de.maxfeigl.loomo.R;

/**
 * Created by Max on 17.01.2018.
 */

public class MyMouth extends Mouth {
    public MyMouth(ImageView mouth, Context context, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super(mouth, context, jsonConverter, mandatoryCheck);
    }

    @Override
    protected void jawDrop() {
        executeMovement(R.drawable.avd_jaw_drop);
    }

    @Override
    protected void jawDropReverse() {
        executeMovement(R.drawable.avd_jaw_drop_reverse);
    }

    @Override
    protected void openMouth() {
        executeMovement(R.drawable.avd_mouth_open);
    }

    @Override
    protected void openMouthReverse() {
        executeMovement(R.drawable.avd_mouth_open_reverse);
    }
}
