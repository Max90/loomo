package de.maxfeigl.emotions.bodyparts;

import android.content.Context;
import android.media.MediaPlayer;
import android.util.Log;

import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 09.01.2018.
 */

public abstract class Voice extends BodyPart {
    private Context context;
    private MediaPlayer mediaPlayer;

    public Voice(Context context, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super();
        this.context = context;
        this.mandatoryCheck = mandatoryCheck;
        super.setAdditionalMandatoryMovements(new ArrayList<String>());
        super.setExecutedMovements(new ArrayList<String>());
        super.setJsonConverter(jsonConverter);
    }


    protected abstract void laugh();

    protected abstract void sad();

    protected abstract void scream();

    protected abstract void surprised();

    protected abstract void concerned();

    protected abstract void unsure();

    protected abstract void ew();

    protected abstract void happy();

    protected abstract void angry();

    protected void playSound(String soundName) {
        int resID = context.getResources().getIdentifier(soundName, "raw", context.getPackageName());
        if (mediaPlayer != null) {
            mediaPlayer.stop();
            mediaPlayer.reset();
        }
        mediaPlayer = MediaPlayer.create(context, resID);
        mediaPlayer.start();
    }


    @Override
    protected void executeFunctionForName(String name) {
        Log.i("movements", name);
        switch (name) {
            case "lachen":
                laugh();
                break;
            case "traurig":
                sad();
                break;
            case "unsicher":
                unsure();
                break;
            case "ängstlich":
                scream();
                break;
            case "überrascht":
                surprised();
                break;
            case "schreien":
                scream();
                break;
            case "ekel":
                ew();
                break;
            case "glücklich":
                happy();
                break;
            case "wut":
                angry();
        }
        super.addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(name);
    }

    @Override
    protected void executeReverseFunctionForName(String name) {

    }

    @Override
    public void setConcreteBodyPart(JSONObject object) {
        super.setBodyPart(object);
    }

    @Override
    protected boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback) {
        switch (name) {
            case "lachen":
                break;
            case "traurig":
                break;
            case "unsicher":
                break;
            case "ängstlich":
                break;
            case "überrascht":
                break;
            case "schreien":
                break;
            default:
                if (checkForFallback)
                    super.checkIfFallbackPossible(name);
                else
                    return false;
                break;
        }
        return true;
    }
}
