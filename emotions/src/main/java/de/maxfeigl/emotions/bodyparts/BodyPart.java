package de.maxfeigl.emotions.bodyparts;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.maxfeigl.emotions.helpers.AppConstants;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;
import de.maxfeigl.emotions.helpers.Util;

/**
 * Created by Max on 14.12.2017.
 */

public abstract class BodyPart {
    public MandatoryCheck mandatoryCheck;
    int movementSpeed = AppConstants.SLOW_MOVEMENT_SPEED;
    int delayBetweenMovements = AppConstants.FEW_MOVEMENTS;
    private ArrayList<String> executedMovements = new ArrayList<>();
    private ArrayList<String> additionalMandatoryMovements = new ArrayList<>();
    private JSONArray instructions;
    private ArrayList<JSONObject> conditions = new ArrayList<>();
    private JSONObject bodyPartObject;
    private int minNumberOfInstructions = 1;
    private int maxNumberOfInstructions = 1;
    private JSONConverter jsonConverter;


    protected abstract void executeFunctionForName(String name);

    protected abstract void executeReverseFunctionForName(String name);

    public abstract void setConcreteBodyPart(JSONObject object);

    protected abstract boolean checkIfMandatoryMovementsPossible(String name, boolean checkForFallback);

    /**
     * checks if the current movement is part of a constraint
     * <p>
     * if current movement is part of an or-condition remove all other movements that are part of the
     * same condition from the list of possible movements
     * <p>
     * if current movement is part of an and-condition add all other movements that are part of the
     * same condition to the list of mandatory movements and remove that condition from the list of
     * conditions to prevent duplicate execution of that movement
     *
     * @param movementName name of the current movement
     * @throws JSONException
     */
    private void checkForConstraints(String movementName, ArrayList<JSONObject> conditions) throws JSONException {
        for (JSONObject conditionObject : conditions) {
            if (conditionObject.has("or")) {
                JSONArray orArray = (JSONArray) conditionObject.get("or");
                for (int i = 0; i < orArray.length(); i++) {
                    if (orArray.get(i).equals(movementName)) {
                        removeConstraintEmotions(orArray, movementName);
                    }
                }
            }
            if (conditionObject.has("and")) {
                JSONArray andArray = (JSONArray) conditionObject.get("and");
                for (int i = 0; i < andArray.length(); i++) {
                    if (andArray.get(i).equals(movementName)) {
                        updateAdditionalMandatoryMovements(andArray, movementName);
                        conditions.remove(conditionObject);
                    }
                }
            }
        }
    }

    /**
     * adds movements that are part of an "and"-constraint to the list of mandatory movements to be
     * executed
     *
     * @param movementsToExecute list of emotions that are part of that constraint
     * @param movementName       name of the movement which triggered that constraint
     */
    private void updateAdditionalMandatoryMovements(JSONArray movementsToExecute, String movementName) {
        for (int i = 0; i < movementsToExecute.length(); i++) {
            try {
                // because we already added the current movement to executedMovements only add all others
                if (!movementsToExecute.get(i).equals(movementName))
                    additionalMandatoryMovements.add((String) movementsToExecute.get(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * removes emotions of "or"-object from list of possible emotions
     * <p>
     * adds the emotions from the "or"-object to executedMovements
     * all emotions in executedMovements are no longer executed
     *
     * @param movementsToLimit list of emotions that are part of that constraint
     * @param movementName     name of the movement which triggered that constraint
     */
    private void removeConstraintEmotions(JSONArray movementsToLimit, String movementName) {
        for (int i = 0; i < movementsToLimit.length(); i++) {
            try {
                // because we already added the current movement to executedMovements only add all others
                if (!movementsToLimit.get(i).equals(movementName)) {
                    int entryToRemove = Util.getIntForString(instructions, movementName);
                    instructions = Util.removeValueFromArray(instructions, entryToRemove);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * adds the executed movement to the array with already executed movements
     * checks for constraints that involve this movement
     * execute all movements related with an "and"-constraint
     *
     * @param name Name of the movement that was executed
     */
    public void addToExecutedMovementsAndExecuteAdditionalMandatoryMovements(String name) {
        executedMovements.add(name);
        try {
            checkForConstraints(name, conditions);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        executeAdditionalMandatoryMovements();
    }

    /**
     * as long as the number of max executed movements for the concrete bodyPart is not reached and
     * there are movements left, that have not been done, perform another movement
     *
     * if maxNumberOfInstructions == -1 perform n movements
     */
    public void executeRandomMovement() {
        while ((maxNumberOfInstructions == -1 || executedMovements.size() < maxNumberOfInstructions || executedMovements.size() < minNumberOfInstructions)
                && (instructions != null && instructions.length() > 0)) {
            int randomInt = Util.randomInt(0, instructions.length() - 1);
            try {
                executeFunctionForName((String) instructions.get(randomInt));
                //removes the executed movement from the list of possible movements to prevent
//                duplicate execution
                instructions = Util.removeValueFromArray(instructions, randomInt);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * iterates over additionalMandatoryMovements and executes them
     */
    private void executeAdditionalMandatoryMovements() {
        for (int i = 0; i < additionalMandatoryMovements.size(); i++) {
            // we need to remove the movement to prevent multiple executions of the same movement
            String movementToExecute = additionalMandatoryMovements.get(i);
            additionalMandatoryMovements.remove(i);
            i--;
            executeFunctionForName(movementToExecute);
        }
    }

    /**
     * sets all objects for the automation of movements
     *
     * @param object JSONObject consisting of
     *               JSONArray movements list of all movements for this concrete bodyPart
     *               JSONArray constraints - list of all constraints
     *               int min
     *               int max
     *               JSONArray mandatory"- list of all mandatory movements
     */
    void setBodyPart(JSONObject object) {
        Movement movement = new Movement();
        try {
            movement.setMovement(jsonConverter.initializeMovement());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        movementSpeed = movement.getMovementSpeed();
        delayBetweenMovements = movement.getDelayBetweenMovements();
        bodyPartObject = object;
        try {
            instructions = (JSONArray) bodyPartObject.get("movements");
            setCardinalities();
            if (bodyPartObject.has("constraints")) {
                JSONArray constraints = (JSONArray) bodyPartObject.get("constraints");
                JSONObject allConditions = jsonConverter.getConditions();

                // get all conditions relevant for eyebrows
                for (int i = 0; i < constraints.length(); i++) {
                    conditions.add((JSONObject) allConditions.get(constraints.get(i).toString()));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * checks if mandatory movements are implemented
     */
    public void checkMandatoryMovements() {
        if (bodyPartObject.has("mandatory")) {
            try {
                JSONArray mandatoryMovements = (JSONArray) bodyPartObject.get("mandatory");
                for (int i = 0; i < mandatoryMovements.length(); i++) {
                    checkIfMandatoryMovementsPossible((String) mandatoryMovements.get(i), true);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * checks if one of the movements in fallbacksArray is executable
     * <p>
     * if it is removes this movement from the array
     */
    public void checkIfFallbackExecuatable() {
        for (int i = 0; i < mandatoryCheck.getFallbackMovements().size(); i++) {
            if (checkIfMandatoryMovementsPossible(mandatoryCheck.getFallbackMovements().get(i), false)) {
                executeFunctionForName(mandatoryCheck.getFallbackMovements().get(i));
                mandatoryCheck.removeFromFallbackMovements(i);
                i--;
            }
        }
    }

    /**
     * checks if there is the possibility of a fallback movement
     * if not sets the emotion as not displayable
     * <p>
     * fallbackArray consists of two entries:
     * fallbackArray[0] = name of the mandatory movement
     * fallbackArray[1] = name of the possible fallback movement
     *
     * @param name name of the mandatory movement to execute
     */
    public void checkIfFallbackPossible(String name) {
        try {
            for (JSONObject conditionObject : conditions) {
                if (conditionObject.has("fallback")) {
                    JSONArray fallbackArray = (JSONArray) conditionObject.get("fallback");
                    if (fallbackArray.get(0).equals(name)) {
                        mandatoryCheck.addToFallbackMovements(fallbackArray.get(1).toString());
                        return;
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mandatoryCheck.setMandatoryFailed(true);
    }

    public void executeMandatoryInstructions() {
        if (bodyPartObject.has("mandatory")) {
            try {
                JSONArray mandatoryMovements = (JSONArray) bodyPartObject.get("mandatory");
                for (int i = 0; i < mandatoryMovements.length(); i++) {
                    executeFunctionForName((String) mandatoryMovements.get(i));
                    int entryToRemove = Util.getIntForString(instructions, (String) mandatoryMovements.get(i));
                    instructions = Util.removeValueFromArray(instructions, entryToRemove);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void reverseExecutedMovements() {
        for (int i = 0; i < executedMovements.size(); i++) {
            executeReverseFunctionForName(executedMovements.get(i));
        }
        executedMovements = new ArrayList<>();
    }

    private void setCardinalities() throws JSONException {
        if (bodyPartObject.has("min"))
            minNumberOfInstructions = (int) bodyPartObject.get("min");

        if (bodyPartObject.has("max"))
            maxNumberOfInstructions = (int) bodyPartObject.get("max");
    }

    void setAdditionalMandatoryMovements(ArrayList<String> additionalMandatoryMovements) {
        this.additionalMandatoryMovements = additionalMandatoryMovements;
    }

    public int getMovementSpeed() {
        return movementSpeed;
    }

    void setMovementSpeed(int movementSpeed) {
        this.movementSpeed = movementSpeed;
    }

    public int getDelayBetweenMovements() {
        return delayBetweenMovements;
    }

    void setDelayBetweenMovements(int delayBetweenMovements) {
        this.delayBetweenMovements = delayBetweenMovements;
    }

    void setJsonConverter(JSONConverter jsonConverter) {
        this.jsonConverter = jsonConverter;
    }

    public ArrayList<String> getExecutedMovements() {
        return executedMovements;
    }

    void setExecutedMovements(ArrayList<String> executedMovements) {
        this.executedMovements = executedMovements;
    }
}
