package de.maxfeigl.loomo.bodyParts;

import android.content.Context;
import android.view.View;

import de.maxfeigl.emotions.bodyparts.Eyebrow;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 17.01.2018.
 */

public class MyEyebrow extends Eyebrow {
    public MyEyebrow(View leftEyebrow, View rightEyebrow, Context context, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super(leftEyebrow, rightEyebrow, context, jsonConverter, mandatoryCheck);
    }

    @Override
    protected void lowerEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().y(leftEyebrow.getY() + getEyebrowLoweringInPx()).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().y(rightEyebrow.getY() + getEyebrowLoweringInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void reverseLowerEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().y(leftEyebrow.getY() - getEyebrowLoweringInPx()).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().y(rightEyebrow.getY() - getEyebrowLoweringInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void raiseEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().y(leftEyebrow.getY() - getEyebrowLoweringInPx()).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().y(rightEyebrow.getY() - getEyebrowLoweringInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void reverseRaiseEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().y(leftEyebrow.getY() + getEyebrowLoweringInPx()).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().y(rightEyebrow.getY() + getEyebrowLoweringInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void contractEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().x(leftEyebrow.getX() + getEyebrowContractionInPx()).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().x(rightEyebrow.getX() - getEyebrowContractionInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void reverseContractEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().x(leftEyebrow.getX() - getEyebrowContractionInPx()).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().x(rightEyebrow.getX() + getEyebrowContractionInPx()).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void raiseInnerEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().rotation(-5).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().rotation(5).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void reverseRaiseInnerEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().rotation(0).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().rotation(0).setDuration(super.getMovementSpeed());
    }

    @Override
    protected void lowerInnerEyebrow(View leftEyebrow, View rightEyebrow) {
        if (leftEyebrow != null)
            leftEyebrow.animate().rotation(5).setDuration(super.getMovementSpeed());
        if (rightEyebrow != null)
            rightEyebrow.animate().rotation(-5).setDuration(super.getMovementSpeed());
    }


}
