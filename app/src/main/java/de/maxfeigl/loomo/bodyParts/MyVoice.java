package de.maxfeigl.loomo.bodyParts;

import android.content.Context;

import de.maxfeigl.emotions.bodyparts.Voice;
import de.maxfeigl.emotions.helpers.JSONConverter;
import de.maxfeigl.emotions.helpers.MandatoryCheck;

/**
 * Created by Max on 17.01.2018.
 */

public class MyVoice extends Voice {
    public MyVoice(Context context, JSONConverter jsonConverter, MandatoryCheck mandatoryCheck) {
        super(context, jsonConverter, mandatoryCheck);
    }

    @Override
    protected void laugh() {
        playSound("laugh");
    }

    @Override
    protected void sad() {
        playSound("sad");
    }

    @Override
    protected void scream() {
        playSound("scream");
    }

    @Override
    protected void surprised() {
        playSound("gasp");
    }

    @Override
    protected void concerned() {
        playSound("concerned");
    }

    @Override
    protected void unsure() {
        playSound("unsicher");
    }

    @Override
    protected void ew() {
        playSound("disgust");
    }

    @Override
    protected void happy() {
        playSound("happy");
    }

    @Override
    protected void angry() {
        playSound("anger");
    }
}
