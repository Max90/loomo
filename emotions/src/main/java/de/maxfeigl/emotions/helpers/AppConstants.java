package de.maxfeigl.emotions.helpers;

/**
 * Created by Max on 05.01.2018.
 */

public class AppConstants {
    public static final int FAST_MOVEMENT_SPEED = 250;
    public static final int SLOW_MOVEMENT_SPEED = 2000;
    public static final int FEW_MOVEMENTS = 3000;
    public static final int MANY_MOVEMENTS = 1000;
}
