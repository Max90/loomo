package de.maxfeigl.loomo;

import com.segway.robot.algo.Pose2D;
import com.segway.robot.sdk.locomotion.sbv.Base;

/**
 * Created by Max on 12.12.2017.
 */

public class Loomo {
    private Base mBase;

    public Loomo(Base mBase) {
        this.mBase = mBase;
    }

    public Pose2D getPose() {
        mBase.cleanOriginalPoint();
        Pose2D pose2D = mBase.getOdometryPose(-1);
        mBase.setOriginalPoint(pose2D);
        return pose2D;
    }
}
