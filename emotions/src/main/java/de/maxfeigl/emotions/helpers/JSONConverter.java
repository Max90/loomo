package de.maxfeigl.emotions.helpers;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by Max on 12.12.2017.
 */

public class JSONConverter {

    private Context context;
    private JSONObject object;
    private JSONObject face;
    private JSONObject eyebrow;
    private JSONObject conditions;

    public JSONConverter(Context context) {
        this.context = context;
    }

    public JSONObject getJSONfromFile(String fileName, String emotionName) throws IOException, JSONException {
        InputStream is = context.getResources().openRawResource(context.getResources().getIdentifier(
                fileName, "raw", context.getPackageName()));
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();
        String myJson = new String(buffer, "UTF-8");
        object = new JSONObject(myJson);
        object = (JSONObject) object.get(emotionName);
        conditions = (JSONObject) object.get("conditions");
        return object;
    }

    public JSONObject initializeFace() throws JSONException {
        face = (JSONObject) object.get("gesicht");
        return face;
    }

    public JSONObject initializeEyebrow() throws JSONException {
        return (JSONObject) face.get("augenbrauen");

    }

    public JSONObject initilizeEye() throws JSONException {
        return (JSONObject) face.get("augen");
    }

    public JSONObject initializeWheels() throws JSONException {
        return (JSONObject) object.get("ganzer Körper");
    }

    public JSONObject initializeMouth() throws JSONException {
        return (JSONObject) face.get("mund");
    }

    public JSONObject initializeLip() throws JSONException {
        return (JSONObject) face.get("lippen");
    }

    public JSONObject initializeEyelid() throws JSONException {
        return (JSONObject) face.get("augenlid");
    }

    public JSONObject getConditions() throws JSONException {
        return conditions;
    }

    public JSONArray initializeMovement() throws JSONException {
        return (JSONArray) object.get("bewegungsart");
    }

    public JSONObject initializeHead() throws JSONException {
        return (JSONObject) object.get("kopf");
    }

    public JSONObject initializeVoice() throws JSONException {
        return (JSONObject) object.get("sprache");
    }

    public JSONArray getMandatoryBodyParts() throws JSONException {
        return (JSONArray) object.get("mandatory");
    }
}
